package aplicacionmascota.dreampet.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentManager;

public class MensajeDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle estado){
        AlertDialog.Builder mensaje =new AlertDialog.Builder(getActivity());
        mensaje.setTitle("Confirmación");
        mensaje.setMessage("La acción eliminará todos los registros asociados. ¿Desea continuar?");
        mensaje.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getActivity(),"Se ha eliminado el registro correctamente",Toast.LENGTH_SHORT).show();
            }
        });
        mensaje.setNegativeButton("No deseo", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getActivity(),"Excelente desición", Toast.LENGTH_SHORT).show();
            }
        });
        return mensaje.create();

    }


}
