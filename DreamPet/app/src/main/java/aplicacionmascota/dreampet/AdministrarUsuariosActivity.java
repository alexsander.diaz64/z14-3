package aplicacionmascota.dreampet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class AdministrarUsuariosActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_DreamPet);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_administrar_usuarios);

        Button btnAgregarUsuario = findViewById(R.id.AdministrarUsuario_btnAgregarUsuario);
        btnAgregarUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(),FormularioRegistroActivity.class);
                startActivity(intent);
            }
        });

        Button btnBuscarUsuario = findViewById(R.id.AdministrarUsuario_btnBuscarUsuario);
        btnBuscarUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), BuscarUsuarioActivity.class);
                startActivity(intent);
            }
        });

        Button btnListar = findViewById(R.id.AdministrarUsuario_btnListarUsuario);
        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ListadoUsuariosActivity.class);
                startActivity(intent);
            }
        });

        /*Button btnListarRecycler = findViewById(R.id.adminUsuarios_btnListarRecycler);
        btnListarRecycler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ListarUsuariosRecyclerActivity.class);
                startActivity(intent);
            }
        });*/
    }
}