package aplicacionmascota.dreampet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme((R.style.Theme_DreamPet));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Bundle datos = getIntent().getExtras();
        if(datos!=null)
        {
            TextView usuario = findViewById(R.id.main_txtUsuario);
            usuario.setText(datos.getString("usuario"));
        }

        Button btnRegistrarMascota = findViewById(R.id.AdministrarUsuario_btnAgregarUsuario);
        btnRegistrarMascota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), ListadoMascotasActivity.class);
                startActivity(i);
            }
        });

        Button btnAdministrarUsuarios = findViewById(R.id.main_btnUsuariosRegistrados);
        btnAdministrarUsuarios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(),AdministrarUsuariosActivity.class);
                startActivity(intent);
            }
        });


        ImageButton ibtSalir = findViewById((R.id.main_ibtSalir));
        ibtSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }
}