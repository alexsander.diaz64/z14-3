package aplicacionmascota.dreampet;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

import aplicacionmascota.dreampet.adapters.UsuariosAdapter;
import aplicacionmascota.dreampet.dao.UsuarioDAO;
import aplicacionmascota.dreampet.modelos.Usuario;

public class ListadoUsuariosActivity extends AppCompatActivity {

    private ListView listaUsuarios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_DreamPet);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_usuarios);

        this.listaUsuarios = findViewById(R.id.listarUsuarios_lista);

        UsuarioDAO basedatos = new UsuarioDAO(this);
        ArrayList<Usuario> listado = basedatos.listar(null);
        UsuariosAdapter adaptador = new UsuariosAdapter(this, R.layout.item_adapter_usuario, listado);
        listaUsuarios.setAdapter(adaptador);


    }
}