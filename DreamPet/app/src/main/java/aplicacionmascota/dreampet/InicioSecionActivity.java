package aplicacionmascota.dreampet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class InicioSecionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme((R.style.Theme_DreamPet));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_secion);

        Button btnInicio = findViewById(R.id.inicio_bntInicio);
        btnInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), LoginActivity.class);
                startActivity(i);
            }
        });

        Button btnRegistrar =findViewById(R.id.inicio_btnRegistrar);
        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),FormularioRegistroActivity.class);
                startActivity(i);
            }
        });
    }
}