package aplicacionmascota.dreampet.clases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class SqliteConex extends SQLiteOpenHelper {

    private static final String baseDatosusuarios = "dbregistrousuarios";
    private static final int version =1;

    public SqliteConex(@Nullable Context context) {
        super(context, baseDatosusuarios, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE usuarios (\n" +
                "    id        INTEGER      PRIMARY KEY AUTOINCREMENT NOT NULL,\n" +
                "    tipdoc    VARCHAR (2)  NOT NULL,\n" +
                "    documento VARCHAR (11) NOT NULL,\n" +
                "    nombres   VARCHAR (50) NOT NULL,\n" +
                "    apellidos VARCHAR (50) NOT NULL,\n" +
                "    usuarios  VARCHAR (20) NOT NULL,\n" +
                "    clave     VARCHAR (80) NOT NULL\n" +
                ")");
        db.execSQL("INSERT INTO `usuarios` (`tipdoc`,`documento`,`nombres`,`apellidos`,`usuarios`,`clave`)\n" +
                "VALUES\n" +
                "  (\"CC\",\"10425874\",\"Dennis\",\"Alfredo\",\"Beasley, Jayme M.\",\"1234\");" +
                "");


        db.execSQL("CREATE TABLE `mascota` (\n" +
                "  id        INTEGER      PRIMARY KEY AUTOINCREMENT NOT NULL, \n" +
                "  nombre varchar(255) default NULL,\n" +
                "  Estado varchar(255) default NULL,\n" +
                "  especie varchar(255) default NULL,\n" +
                "  fecha_de_nacimiento varchar(255),\n" +
                "  raza varchar(255) default NULL,\n" +
                "  direccion varchar(255) default NULL,\n" +
                " coordenadas varchar(255) default NULL\n" +
                ")");

        db.execSQL("INSERT INTO `mascota` (`nombre`,`Estado`,`especie`,`fecha_de_nacimiento`,`raza`,`direccion`)\n" +
                "VALUES\n" +
                "  (\"Amelia Farrell\",\"Yes\",\"Ivor\",\"Nov 30, 2014\",\"Hubbard, Cameran D.\",\"Ap #238-3093 Quis, Rd.\"),\n" +
                "  (\"Jayme Goodwin\",\"No\",\"Solomon\",\"Jun 8, 2016\",\"Houston, Damian J.\",\"838-5475 Vitae Street\"),\n" +
                "  (\"Pamela Miles\",\"Yes\",\"Lawrence\",\"Jan 19, 2015\",\"Hill, Dora B.\",\"6776 Ut Av.\"),\n" +
                "  (\"Ethan Morin\",\"No\",\"Walter\",\"Oct 6, 2018\",\"Rasmussen, Russell D.\",\"Ap #995-1219 Velit Ave\"),\n" +
                "  (\"Cade Smith\",\"No\",\"Ulysses\",\"Oct 11, 2018\",\"Fry, Jerome B.\",\"Ap #940-3764 Adipiscing Road\"),\n" +
                "  (\"Zenaida Hays\",\"No\",\"Alexander\",\"May 17, 2020\",\"Wade, Zelenia I.\",\"133-2859 Ac Rd.\"),\n" +
                "  (\"Chelsea Meyer\",\"Yes\",\"Harper\",\"Sep 10, 2016\",\"Mcfadden, Regina X.\",\"P.O. Box 904, 3348 Magna. Ave\"),\n" +
                "  (\"Alma Ortega\",\"No\",\"Jerry\",\"Jan 13, 2012\",\"Jimenez, MacKenzie M.\",\"Ap #548-536 Tincidunt Avenue\"),\n" +
                "  (\"Gil Ayala\",\"Yes\",\"Chester\",\"Sep 21, 2012\",\"Bates, Ursa E.\",\"6405 Feugiat. Avenue\"),\n" +
                "  (\"Bethany Meyer\",\"Yes\",\"Mark\",\"May 25, 2020\",\"Conley, Ronan Y.\",\"Ap #380-7132 Amet, Ave\"),\n" +
                "  (\"Jermaine Marshall\",\"No\",\"Josiah\",\"Dec 14, 2012\",\"Patterson, Candice X.\",\"P.O. Box 933, 7237 Vitae, Street\"),\n" +
                "  (\"Jin Love\",\"No\",\"Ferris\",\"Jan 8, 2021\",\"Brooks, Eleanor J.\",\"602-2642 A St.\"),\n" +
                "  (\"Sigourney Elliott\",\"No\",\"Macaulay\",\"Mar 24, 2011\",\"Rosa, Laurel F.\",\"272-2467 Ac Rd.\"),\n" +
                "  (\"Cole Bass\",\"No\",\"Brandon\",\"May 29, 2015\",\"Carson, Shaeleigh I.\",\"485-5443 Dis Ave\"),\n" +
                "  (\"Evelyn Silva\",\"Yes\",\"Scott\",\"Jan 21, 2022\",\"Vargas, Emerson B.\",\"Ap #311-6610 Tempus St.\"),\n" +
                "  (\"Ursula Coleman\",\"No\",\"Hoyt\",\"Mar 23, 2014\",\"Welch, Rhea Q.\",\"278-5463 Ipsum Av.\"),\n" +
                "  (\"Joel Sexton\",\"No\",\"Silas\",\"Dec 5, 2022\",\"Hall, Lila Q.\",\"133-7393 Blandit Rd.\"),\n" +
                "  (\"Imelda Castro\",\"No\",\"Richard\",\"Mar 26, 2008\",\"Mcgee, Beau H.\",\"Ap #427-514 Hendrerit. Rd.\"),\n" +
                "  (\"Ferdinand Guerra\",\"Yes\",\"Erasmus\",\"Sep 10, 2011\",\"Glenn, Haviva N.\",\"752-9957 Massa. Av.\"),\n" +
                "  (\"Silas Todd\",\"Yes\",\"Baxter\",\"Jan 16, 2020\",\"Puckett, Joan T.\",\"716-2991 Eu, Rd.\"),\n" +
                "  (\"Warren Schmidt\",\"No\",\"Maxwell\",\"Feb 29, 2012\",\"Riggs, Inga R.\",\"701-5956 Facilisis. Rd.\"),\n" +
                "  (\"Kaseem Ray\",\"No\",\"Raymond\",\"Aug 19, 2018\",\"Kemp, Rhonda V.\",\"P.O. Box 889, 1855 Amet, Av.\"),\n" +
                "  (\"Shay Foster\",\"Yes\",\"Leo\",\"Jul 10, 2010\",\"Hewitt, Nigel M.\",\"853-5946 Tristique Rd.\"),\n" +
                "  (\"Elton Lott\",\"No\",\"Elmo\",\"Aug 12, 2018\",\"Mcintosh, Rhea T.\",\"Ap #152-3852 Risus. Street\"),\n" +
                "  (\"Lois Woods\",\"Yes\",\"Russell\",\"May 14, 2018\",\"Roman, Christine K.\",\"7631 Lacinia Road\"),\n" +
                "  (\"Hyacinth Ruiz\",\"Yes\",\"Forrest\",\"Oct 2, 2016\",\"Sampson, Gage Q.\",\"P.O. Box 396, 1060 Est Rd.\"),\n" +
                "  (\"Maite Maynard\",\"No\",\"Blake\",\"Apr 20, 2017\",\"Simmons, Brianna Q.\",\"P.O. Box 908, 1431 Blandit Rd.\"),\n" +
                "  (\"Barclay Huffman\",\"Yes\",\"Cody\",\"Oct 22, 2012\",\"Carrillo, Nita D.\",\"P.O. Box 752, 2336 Gravida Avenue\"),\n" +
                "  (\"Tatyana Greene\",\"No\",\"Jesse\",\"Feb 13, 2023\",\"Snider, Yen S.\",\"598-3806 Dictum Ave\"),\n" +
                "  (\"Tashya Burnett\",\"Yes\",\"Caleb\",\"Jun 1, 2022\",\"Freeman, Honorato Y.\",\"963-537 Mauris. Rd.\"),\n" +
                "  (\"Gabriel Gonzales\",\"No\",\"Wyatt\",\"Nov 14, 2016\",\"Ortiz, Flynn M.\",\"Ap #152-9666 Rutrum. St.\"),\n" +
                "  (\"Nicholas Houston\",\"Yes\",\"Valentine\",\"Mar 19, 2008\",\"Byers, Kimberley C.\",\"108-1617 Lectus Rd.\"),\n" +
                "  (\"Irma Stafford\",\"No\",\"Todd\",\"Oct 11, 2012\",\"Robles, Devin V.\",\"Ap #789-2867 Sem Av.\"),\n" +
                "  (\"Breanna Bass\",\"No\",\"Kareem\",\"Sep 15, 2013\",\"Snyder, Blaze R.\",\"P.O. Box 731, 4073 Ornare Street\"),\n" +
                "  (\"Olga Hansen\",\"Yes\",\"Hamish\",\"Jul 20, 2009\",\"Mcfarland, Nissim X.\",\"Ap #254-8517 Lacinia St.\"),\n" +
                "  (\"Selma Harris\",\"Yes\",\"David\",\"Jul 11, 2009\",\"Tyson, Idona V.\",\"Ap #447-3317 Condimentum. Ave\"),\n" +
                "  (\"Stewart Guthrie\",\"Yes\",\"Edan\",\"Nov 15, 2005\",\"Whitaker, Alexis T.\",\"218-1878 Et, Av.\"),\n" +
                "  (\"Brenda Miles\",\"No\",\"Kermit\",\"Jul 31, 2021\",\"Cole, Dalton W.\",\"P.O. Box 666, 4091 Vitae Street\"),\n" +
                "  (\"Rama Miles\",\"No\",\"Ali\",\"Aug 8, 2012\",\"Salazar, Nehru X.\",\"P.O. Box 194, 3202 Sem Street\"),\n" +
                "  (\"Avram Jenkins\",\"No\",\"Kenyon\",\"Dec 13, 2012\",\"Graham, Zachary M.\",\"Ap #562-2522 Massa. Road\"),\n" +
                "  (\"William Parks\",\"No\",\"Cedric\",\"May 12, 2021\",\"Sellers, Sydnee M.\",\"914-5674 In St.\"),\n" +
                "  (\"Arden Guerrero\",\"Yes\",\"Rahim\",\"Jun 23, 2015\",\"West, Amy C.\",\"2214 Pellentesque, St.\"),\n" +
                "  (\"Susan Pitts\",\"Yes\",\"Brennan\",\"Feb 2, 2007\",\"Walker, Yeo S.\",\"Ap #178-8708 Ullamcorper Street\"),\n" +
                "  (\"William Booker\",\"Yes\",\"Igor\",\"Sep 6, 2019\",\"Martin, Rae C.\",\"P.O. Box 955, 5282 Enim, Ave\"),\n" +
                "  (\"Willow Santana\",\"Yes\",\"Kieran\",\"Sep 24, 2012\",\"Orr, Tanek P.\",\"Ap #211-8453 Neque. Road\"),\n" +
                "  (\"Shaeleigh Cleveland\",\"No\",\"Fletcher\",\"Feb 23, 2009\",\"Grimes, Hunter C.\",\"Ap #901-7981 Vestibulum Rd.\"),\n" +
                "  (\"Nathaniel Donovan\",\"Yes\",\"Noble\",\"Aug 3, 2023\",\"Morgan, Quynn U.\",\"268-2570 Dignissim Rd.\"),\n" +
                "  (\"Ciara Hammond\",\"No\",\"Brent\",\"Aug 30, 2023\",\"Rosario, Rhonda F.\",\"Ap #251-6431 Enim Ave\"),\n" +
                "  (\"Bruce Roman\",\"No\",\"Coby\",\"Mar 30, 2009\",\"Nicholson, Colton P.\",\"572-1272 Risus. Rd.\"),\n" +
                "  (\"Rae Sampson\",\"No\",\"Dennis\",\"May 5, 2009\",\"Beasley, Jayme M.\",\"Ap #234-1351 In Ave\");" +
                "");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
