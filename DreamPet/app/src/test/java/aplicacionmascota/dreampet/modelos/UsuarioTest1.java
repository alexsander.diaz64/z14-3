package aplicacionmascota.dreampet.modelos;

import junit.framework.TestCase;

public class UsuarioTest1 extends TestCase {

    public void testSetNombres() {
        String nombre = "Juan";
        Usuario modelo =new Usuario();
        modelo.setNombres(nombre);
        String resultadoObtenido = modelo.getNombres();
        String resultadoEsperado = nombre;

        assertEquals(resultadoEsperado,resultadoObtenido);


    }

    public void testSetApellidos() {
        String apellido= "Garcia";
        Usuario modelo =new Usuario();
        modelo.setApellidos(apellido);
        String resultadoObtenido = modelo.getApellidos();
        String resultadoEsperado = apellido;

        assertEquals(resultadoEsperado,resultadoObtenido);
    }
}