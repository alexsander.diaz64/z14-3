package aplicacionmascota.dreampet;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import aplicacionmascota.dreampet.dao.UsuarioDAO;
import aplicacionmascota.dreampet.modelos.Usuario;

public class FormularioRegistroActivity extends AppCompatActivity {

    private Spinner spnTipDoc;
    private EditText txtDocumento;
    private EditText txtNombres;
    private EditText txtApellidos;
    private EditText txtUsuario;
    private EditText txtClave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme((R.style.Theme_DreamPet));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_registro);

        spnTipDoc = findViewById(R.id.formularioregistro_spntipdoc);
        txtDocumento = findViewById(R.id.formularioregistro_txtDocumento);
        txtNombres= findViewById(R.id.formularioregistro_txtNombres);
        txtApellidos = findViewById(R.id.formularioregistro_txtApellidos);
        txtUsuario = findViewById(R.id.formularioregistro_txtUsuario);
        txtClave = findViewById(R.id.formularioregistro_txtClave);

        String[] tiposDoc = { "CC", "TI", "RC", "PA" };
        ArrayAdapter adaptador = new ArrayAdapter(this, android.R.layout.simple_list_item_checked, tiposDoc);
        spnTipDoc.setAdapter(adaptador);

        Button btnGuardar = findViewById(R.id.buscarUsuario_btnActualizarDatos);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Usuario modeloCargado = cargarModelo();
                if(modeloCargado!=null)
                {
                    UsuarioDAO baseDatos = new UsuarioDAO(v.getContext());
                    if(baseDatos.insertar(modeloCargado)>0) {
                        limpiarFormulario();
                        Toast.makeText(v.getContext(), "Se inserto el registro correctamente.", Toast.LENGTH_SHORT).show();
                    }
                    else
                        Toast.makeText(v.getContext(), "Se producido un error al intentar insertar el registro.", Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(v.getContext(), "Digigencie todos los campos en blanco.", Toast.LENGTH_SHORT).show();
            }

        });

    }

    private Usuario cargarModelo()
    {
        Usuario modelo = null;

        String tipdoc = spnTipDoc.getSelectedItem().toString();
        String documento = txtDocumento.getText().toString();
        String nombres = txtNombres.getText().toString();
        String apellidos = txtApellidos.getText().toString();
        String usuario = txtUsuario.getText().toString();
        String clave = txtClave.getText().toString();

        if(!tipdoc.isEmpty() && !documento.isEmpty() && !nombres.isEmpty() && !apellidos.isEmpty() && !usuario.isEmpty() && !clave.isEmpty())
            modelo = new Usuario(tipdoc, documento, nombres, apellidos, usuario, clave);

        return modelo;
    }

    private void limpiarFormulario()
    {
        spnTipDoc.setSelection(0);
        txtDocumento.setText("");
        txtNombres.setText("");
        txtApellidos.setText("");
        txtUsuario.setText("");
        txtClave.setText("");

    }

}