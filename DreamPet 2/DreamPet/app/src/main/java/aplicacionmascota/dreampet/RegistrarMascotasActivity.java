package aplicacionmascota.dreampet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import aplicacionmascota.dreampet.dao.MascotaDAO;
import aplicacionmascota.dreampet.dao.UsuarioDAO;
import aplicacionmascota.dreampet.dialogs.MensajeDialog;
import aplicacionmascota.dreampet.modelos.Mascota;
import aplicacionmascota.dreampet.modelos.Usuario;

public class RegistrarMascotasActivity extends AppCompatActivity {

   private EditText id;
    private EditText nombre;
    private EditText estado;
    private EditText especie;
    private EditText fecha_de_nacimiento;
    private EditText raza;
    private EditText direccion;
    private TextView titulo;
    private int codigoRegistro;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme((R.style.Theme_DreamPet));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_mascotas);


        id = findViewById(R.id.registrarmascota_txtid);
        nombre = findViewById(R.id.registrarmascota_txtnombre);
        estado = findViewById(R.id.registrarmascota_txtestado);
        especie = findViewById(R.id.registrarmascota_txtespecie);
        fecha_de_nacimiento = findViewById(R.id.registrarmascota_txtFechaNacimiento);
        raza = findViewById(R.id.registrarmascota_txtraza);
        direccion = findViewById(R.id.registrarmascota_txtdireccion);
        titulo = findViewById(R.id.registrarmascota_titulo);


        Bundle datos = getIntent().getExtras();
        Button btnEliminar = findViewById(R.id.registrarmascota_btneliminar);
        if (datos != null) {
            codigoRegistro = datos.getInt("id");
            MascotaDAO basedatos = new MascotaDAO(this);
            Mascota registro = basedatos.listar(String.valueOf(codigoRegistro)).get(0);

            id.setText(String.valueOf(registro.getId()));
            nombre.setText(registro.getNombre());
            estado.setText(registro.getEstado());
            especie.setText(registro.getEspecie());
            fecha_de_nacimiento.setText(registro.getFecha_de_nacimiento());
            raza.setText(registro.getRaza());
            direccion.setText(registro.getDireccion());
            titulo.setText(registro.getNombre());


            btnEliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    MascotaDAO baseDatos = new MascotaDAO(v.getContext());
                    baseDatos.eliminar(registro);
                    onBackPressed();
                    finish();
                }
            });
        } else
            btnEliminar.setVisibility(View.GONE);





       /* Button ibtEliminar = findViewById(R.id.registrarmascota_btneliminar);
        ibtEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MensajeDialog mensaje = new MensajeDialog();
                mensaje.show(getFragmentManager(),null);


               // Usuario registro = new Usuario ("CC", "1098643763", "Diana", "Afanador","daafanardor", "112345");
                //System.out.println(registro.toString());


            }
        });

        Button btnRegistrarNuevasMascotas = findViewById(R.id.registrarmascota_btnguardar);
        btnRegistrarNuevasMascotas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               UsuarioDAO usuarioDao = new UsuarioDAO((v.getContext()));

                Usuario objeto = new Usuario(2,"CC", "103256546","laika","salamanca","laika88","12345");

     //           usuarioDao.insertar(objeto);

                ArrayList<Usuario> usuariosGuardados = usuarioDao.listar();

                usuarioDao.eliminar(objeto);

                usuariosGuardados = usuarioDao.listar();

            }
        });*/

        Button btnVolver = findViewById(R.id.registrarmascota_btnvolver);
        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Button btnGuardar = findViewById(R.id.registrarmascota_btnguardar);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Mascota objetoCargado = new Mascota(
                        codigoRegistro,
                        id.getText().toString(),
                        nombre.getText().toString(),
                        estado.getText().toString(),
                        especie.getText().toString(),
                        fecha_de_nacimiento.getText().toString(),
                        raza.getText().toString(),
                        direccion.getText().toString()
                );
                MascotaDAO baseDatos = new MascotaDAO(v.getContext());


                if (codigoRegistro > 0) {
                    baseDatos.actualizar(objetoCargado);
                } else {
                    baseDatos.insertar(objetoCargado);

                    onBackPressed();
                    finish();
                }
            }
        });
    }

}