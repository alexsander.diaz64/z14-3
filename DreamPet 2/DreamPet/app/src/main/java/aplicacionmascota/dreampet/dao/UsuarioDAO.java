package aplicacionmascota.dreampet.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import aplicacionmascota.dreampet.clases.SqliteConex;
import aplicacionmascota.dreampet.modelos.Usuario;

public class UsuarioDAO {
    private Context contexto;
    private final String nombreTabla = "usuarios";
    private SqliteConex conexion;

    public UsuarioDAO(Context contexto) {
        this.contexto = contexto;
        this.conexion = new SqliteConex(this.contexto);
    }

    public long insertar (Usuario modeloUsuario)
    {
        SQLiteDatabase baseDatos = this.conexion.getWritableDatabase();


        ContentValues valoresAInsertar = new ContentValues();
        valoresAInsertar.put( "tipdoc", modeloUsuario.getTipdoc());
        valoresAInsertar.put("documento",modeloUsuario.getDocumento());
        valoresAInsertar.put("nombres",modeloUsuario.getNombres());
        valoresAInsertar.put("apellidos",modeloUsuario.getApellidos());
        valoresAInsertar.put("usuarios",modeloUsuario.getUsuarios());
        valoresAInsertar.put("clave",modeloUsuario.getClave());


        return baseDatos.insert(this.nombreTabla,null,valoresAInsertar);
    }

    public ArrayList<Usuario>listar(String documento)
    {
        //Se crean las variables y se inicializan
        SQLiteDatabase baseDatos = this.conexion.getReadableDatabase();
        ArrayList<Usuario> registros = new ArrayList<Usuario>();

        //Se determinan los campos a consultar por medio de la consulta.
        String[] columnasAConsultar = { "id", "tipdoc", "documento", "nombres", "apellidos", "usuarios", "clave" };

        String condicionWhere = null;
        String[] valoresCondicionWhere = null;
        if(documento!=null && !documento.isEmpty())
        {
            condicionWhere = "documento = ?";
            valoresCondicionWhere = new String[] { documento };
        }

        //Se ejecuta la consulta y se almacena el cursor
        Cursor cursor = baseDatos.query(this.nombreTabla, columnasAConsultar,condicionWhere, valoresCondicionWhere, null, null, null);

        //Se verifica que la consulta haya sido satisfactoria
        if(cursor!=null)
        {
            //Se posiciona en el primer item de la tabla el cursor
            cursor.moveToFirst();

            do {

                //Se crea el objeto registro de tipo usuario
                Usuario registro = new Usuario(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6)
                );
                //Se añade el registro a la lista de usuarios
                registros.add(registro);
                //Se verifica que el cursor pueda desplpazarse al siguiente item,
                //si no se puede, se asume que ha llegado al final del conjunto de registros.
            } while (cursor.moveToNext());

            //Se cierra el cursor
            cursor.close();
        }

        //Se devuelve el listado de todos los usuarios obtenidos en la consulta.
        return registros;
    }

    public int actualizar(Usuario modeloUsuario)
    {
        SQLiteDatabase baseDatos = this.conexion.getWritableDatabase();

        ContentValues valoresAActualizar = new ContentValues();
        valoresAActualizar.put("tipdoc", modeloUsuario.getTipdoc());
        valoresAActualizar.put("documento", modeloUsuario.getDocumento());
        valoresAActualizar.put("nombres", modeloUsuario.getNombres());
        valoresAActualizar.put("apellidos", modeloUsuario.getApellidos());
        valoresAActualizar.put("usuario", modeloUsuario.getUsuarios());
        valoresAActualizar.put("clave", modeloUsuario.getClave());

        String criterioWhere = "id = ?";
        String[] valoresCriterioWhere = { String.valueOf(modeloUsuario.getId()) };

        return baseDatos.update(this.nombreTabla, valoresAActualizar, criterioWhere, valoresCriterioWhere);
    }

    public int eliminar(Usuario modeloUsuario)
    {
        SQLiteDatabase baseDatos = this.conexion.getWritableDatabase();

        String criterioWhere = "id = ?";
        String[] valoresCriterioWhere = { String.valueOf(modeloUsuario.getId()) };

        return baseDatos.delete(this.nombreTabla, criterioWhere, valoresCriterioWhere);
    }
}