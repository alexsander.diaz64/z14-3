package aplicacionmascota.dreampet.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import aplicacionmascota.dreampet.clases.SqliteConex;
import aplicacionmascota.dreampet.modelos.Mascota;


public class MascotaDAO {
    private Context contexto;
    private final String nombreTabla = "mascota";
    private SqliteConex conexion;

    public MascotaDAO(Context contexto) {
        this.contexto = contexto;
        this.conexion = new SqliteConex(this.contexto);
    }

    public long insertar (Mascota modeloMascota)
    {
        SQLiteDatabase baseDatos = this.conexion.getWritableDatabase();


        ContentValues valoresAInsertar = new ContentValues();
        valoresAInsertar.put( "nombre", modeloMascota.getNombre());
        valoresAInsertar.put("estado",modeloMascota.getEstado());
        valoresAInsertar.put("especie",modeloMascota.getEspecie());
        valoresAInsertar.put("fecha_de_nacimiento",modeloMascota.getFecha_de_nacimiento());
        valoresAInsertar.put("raza",modeloMascota.getRaza());
        valoresAInsertar.put("direccion",modeloMascota.getDireccion());
        valoresAInsertar.put("coordenadas",modeloMascota.getCoordenadas());

        return baseDatos.insert(this.nombreTabla,null,valoresAInsertar);
    }

    public ArrayList<Mascota>listar(String documento)
    //Se crean las variables y se inicializan, metodo lista
    {
        SQLiteDatabase baseDatos = this.conexion.getReadableDatabase();
        baseDatos = this.conexion.getReadableDatabase();
        ArrayList<Mascota> registros = new ArrayList<Mascota>();

        // se determinan los campos a consultar
        String [] columnasAConsultar = {"id","nombre","estado", "especie","fecha_de_nacimiento","raza","direccion","coordenadas"};

         String condicionWhere = null;
         String[] valoresCondicionWhere = null;
        if(documento!=null && !documento.isEmpty())
        {
           condicionWhere = "nombre = ?";
           valoresCondicionWhere = new String[] { documento };
        }



// se ejecuta la consulta y se almacena el cursor
        Cursor cursor = baseDatos.query(this.nombreTabla, columnasAConsultar,null,null,null,null,null);
//se verifica que la consulta haya sido satisfactoria
        if (cursor!=null)
        {
            //Se posiciona en el primer itam de la tabla el cursor
            cursor.moveToFirst();

            do{
               /*
               public Mascota(int id, String tipdoc, String documento, String nombres, String apellidos, String Mascotas, String clave)
                */
                Mascota registro = new Mascota(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7)

                );
                registros.add(registro);
            }while (cursor.moveToNext());

            cursor.close();
        }
        return registros;
    }

    public int actualizar (Mascota modeloMascota){

        SQLiteDatabase baseDatos = this.conexion.getWritableDatabase();

        ContentValues valoresAActualizar = new ContentValues();
        valoresAActualizar.put( "nombre", modeloMascota.getNombre());
        valoresAActualizar.put("estado",modeloMascota.getEstado());
        valoresAActualizar.put("especie",modeloMascota.getEspecie());
        valoresAActualizar.put("fecha_de_nacimiento",modeloMascota.getFecha_de_nacimiento());
        valoresAActualizar.put("raza",modeloMascota.getRaza());
        valoresAActualizar.put("direccion",modeloMascota.getDireccion());
        valoresAActualizar.put("coordenadas",modeloMascota.getCoordenadas());

        String criterioWhere = "id = ?";
        String[] valoresCriterioWhere = {String.valueOf(modeloMascota.getId()) };


        return baseDatos.update(this.nombreTabla,valoresAActualizar, criterioWhere, valoresCriterioWhere);
    }

    public int eliminar(Mascota modeloMascota)
    {
        SQLiteDatabase baseDatos = this.conexion.getWritableDatabase();

        String criterioWhere = "id = ?";
        String[] valoresCriterioWhere = {String.valueOf(modeloMascota.getId())};

        return baseDatos.delete(this.nombreTabla,criterioWhere,valoresCriterioWhere);
    }
}