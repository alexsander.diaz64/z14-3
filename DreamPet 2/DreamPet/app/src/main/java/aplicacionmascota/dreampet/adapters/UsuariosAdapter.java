package aplicacionmascota.dreampet.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


import org.w3c.dom.Text;

import java.util.List;

import aplicacionmascota.dreampet.R;
import aplicacionmascota.dreampet.modelos.Usuario;

public class UsuariosAdapter extends ArrayAdapter<Usuario> {

    private List<Usuario> listado;
    private Context contexto;
    private int layoutUtilizado;

    public UsuariosAdapter(@NonNull Context contexto, int layoutUtilizado, List<Usuario> listado) {
        super(contexto, layoutUtilizado, listado);
        this.listado = listado;
        this.contexto = contexto;
        this.layoutUtilizado = layoutUtilizado;
    }

   @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @Nullable ViewGroup parent)
    {
        View vista = convertView;

        if(vista==null)
        {
            vista = LayoutInflater.from(this.contexto).inflate(R.layout.item_adapter_usuario, null);

            Usuario registro = this.listado.get(position);

            TextView txvNombreCompleto = vista.findViewById(R.id.adapterUsuario_nombre);
            TextView txvDireccion = vista.findViewById(R.id.adapterUsuario_direccion);
            TextView txvEmail = vista.findViewById(R.id.adapterUsuario_email);

            txvNombreCompleto.setText(registro.getNombres());
            txvDireccion.setText(registro.getClave());
            txvEmail.setText(registro.getDocumento());

        }
        return vista;
    }

}
